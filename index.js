//import thử viện express js
const express = require ("express");
//khởi tạo app express
const app = express();
// khai báo mongoose 
var mongoose = require('mongoose');
//khởi tạo path
const path = require("path");
//khai báo cổng chạy app
const port =  8000;
//khai báo router sử dụng
const userRouter = require("./app/routes/userRouter");
const diceHistoryRouter = require("./app/routes/diceHistoryRouter");
const voucherHistoryRouter = require("./app/routes/voucherHistoryRouter");
const prizeHistoryRouter = require("./app/routes/prizeHistoryRouter");
const prizeRouter = require("./app/routes/prizeRouter")
const voucherRouter = require("./app/routes/voucherRouter");
const devcampLuckyDiceRouter = require("./app/routes/devcampLuckyDiceRouter")
//kết nối với mongoDB
mongoose.connect('mongodb://127.0.0.1:27017/CRUD_luckyDiceCasino', function(error){
    if(error) throw error;
    console.log('Successfully connected to database');
});
//cấu hình request đọc 
app.use(express.json());
//app sử dụng router
app.use("/", userRouter);
app.use("/", diceHistoryRouter);
app.use("/", voucherHistoryRouter);
app.use("/", prizeHistoryRouter);
app.use("/", prizeRouter);
app.use("/", voucherRouter);
app.use("/", devcampLuckyDiceRouter);
app.use(express.static(__dirname + "/app/views"));
app.get("/", function(request, response) {
    response.sendFile(path.join(__dirname + "/app/views/LuckyDice.html"))
})
//chạy app trên cổng
app.listen(port, () => {
    console.log("App listening on port:" , port);
})



