// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import prizeHistory controllers
const prizeHistoryController = require('../controller/prizeHistoryController');

router.post('/prize-history', prizeHistoryController.createPrizeHistory);

router.get('/prize-history', prizeHistoryController.getAllPrizeHistory);

router.get("/prize-history/:prizeHistoryId", prizeHistoryController.getPrizeHistoryByID);

router.put("/prize-history/:prizeHistoryId", prizeHistoryController.updatePrizeHistoryById);

router.delete("/prize-history/:prizeHistoryId", prizeHistoryController.deletePrizeHistoryById);

module.exports = router;