// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import diceHistory controllers
const diceController = require('../controller/diceController');

router.post('/devcamp-lucky-dice/dice', diceController.diceHandler);
router.get("/devcamp-lucky-dice/dice-history", diceController.getDiceHistory)
router.get("/devcamp-lucky-dice/prize-history", diceController.getPrizeHistory)
router.get("/devcamp-lucky-dice/voucher-history", diceController.getVoucherHistory)

module.exports = router;