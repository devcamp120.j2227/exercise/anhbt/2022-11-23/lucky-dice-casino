// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import user controllers
const voucherController = require('../controller/voucherController');

router.post('/voucher', voucherController.createVoucher);

router.get('/voucher', voucherController.getAllVoucher);

router.get("/voucher/:voucherId", voucherController.getVoucherByID);

router.put("/voucher/:voucherId", voucherController.updateVoucherById);

router.delete("/voucher/:voucherId", voucherController.deleteVoucherById);

module.exports = router;