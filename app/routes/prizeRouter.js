// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import user controllers
const prizeController = require('../controller/prizeController');

router.post('/prize', prizeController.createPrize);

router.get('/prize', prizeController.getAllPrize);

router.get("/prize/:prizeId", prizeController.getPrizeByID);

router.put("/prize/:prizeId", prizeController.updatePrizeById);

router.delete("/prize/:prizeId", prizeController.deleteDiceById);

module.exports = router;
