// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import diceHistory controllers
const diceHistoryController = require('../controller/diceHistoryController');

router.post('/dice', diceHistoryController.creatediceHistory);

router.get('/dice', diceHistoryController.getAllDiceHistory);

router.get("/dice/:diceHistoryId", diceHistoryController.getDiceHistoryByID);

router.put("/dice/:diceHistoryId", diceHistoryController.updateDiceHistoryById);

router.delete("/dice/:diceHistoryId", diceHistoryController.deleteDiceHistoryById);

module.exports = router;