// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import voucherHistory controllers
const voucherHistoryController = require('../controller/voucherHistoryController');

router.post('/voucher-histories', voucherHistoryController.createVoucherHistory);

router.get('/voucher-histories', voucherHistoryController.getAllVoucherHistory);

router.get("/voucher-histories/:voucherHistoryId", voucherHistoryController.getVoucherHistoryByID);

router.put("/voucher-histories/:voucherHistoryId", voucherHistoryController.updateVoucherHistoryById);

router.delete("/voucher-histories/:voucherHistoryId", voucherHistoryController.deleteVoucherHistoryById);

module.exports = router;