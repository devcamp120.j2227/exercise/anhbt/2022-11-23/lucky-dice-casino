// Import thư viện Mongoose
const mongoose = require("mongoose");
// Import module UserModel 
const prizeHistoryModel = require('../models/prizeHistoryModel');

/*** Get all prize: lấy danh sách quay xúc xắc
 * nhận đầu vào request và response từ router
 * gọi prizeHistoryModel lấy tất cả dữ liệu user từ CSDL và trả về response.
*/
const getAllPrizeHistory = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let userId = request.params.user;
    let condition = { };
    console.log(userId)
    if (userId) {
        condition.user = userId;
    }
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    prizeHistoryModel.find(condition).exec((error, data)  => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all prize history successfully",
            data: data
        })
    })
}
//   create prize history : tạo lịch sử xúc xắc
//   input: request body json
//   output: tạo dữ liệu prizeHistory và lưu vào CSDL và trả về response 
const createPrizeHistory = (request, response) => {
    // B1: chuẩn bị dữ liệu
    const body = request.body;  
    // {
    //     "user": "637c907a3d1daf3956ad209a",
    //     "prize" : 12
    // }
    // B2: kiểm tra dữ liệu
    if(!body.user || body.user.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "User  không hợp lệ!"
        })
    }
    if(isNaN(body.prize) || body.prize < 0) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Prize không hợp lệ!"
        })
    }
    // B3: Thao tác CSDL
    //Tạo đối tượng chứa request
    const newPrizeHistory = {
        user: body.user,
        prize: body.prize
    }
    // Dùng create() thêm dữ liệu vào CSDL
    prizeHistoryModel.create(newPrizeHistory, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        //trả về response
        return response.status(201).json({
            status: "Successfully create a prize history.",
            data: data
        })
    })
}
//   get prize history by id : lấy lịch sử xúc xắc theo id
//   input: request params
//   output: lấy dữ liệu từ csdl và reponse
const getPrizeHistoryByID =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const prizeHistoryID =  request.params.prizeHistoryId
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeHistoryID)){
     return response.status(400).json({
             status: "Bad request",
             message: "prizeHistory Id không hợp lệ" 
         })
     }
     //bước 3: gọi model tạo dữ liêu
    prizeHistoryModel.findById(prizeHistoryID,(error,data) =>{
         return response.status(200).json({
             status: "Get detail prize history successfull",
             data: data
             })
         }
     )  
}
//   update prize history by id : lấy lịch sử xúc xắc theo id
//   input: request params
//   output: update dữ liệu từ csdl và reponse
const updatePrizeHistoryById =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const prizeHistoryID =  request.params.prizeHistoryId;
    const body = request.body;
    //bước 2: Validate dữ liệu
        if(!mongoose.Types.ObjectId.isValid(prizeHistoryID)){
            return response.status(400).json({
                    status: "Bad request",
                    message: "Prize History Id không hợp lệ" 
                })
            }  
        if(body.user == undefined && body.user.trim() === ""){
            return response.status(400).json({
                status: "BAD REQUEST",
                message: "User không hợp lệ" 
            })
        }
        if(body.prize == undefined && (isNaN(body.prize) || body.prize < 0)){
            return response.status(400).json({
                status: "BAD REQUEST",
                message: "Prize không hợp lệ" 
            })
        }  
      const updateprizeHistory= {
        user: body.user,
        prize: body.prize
        }
        //bước 3: gọi model tạo dữ liêu
        prizeHistoryModel.findByIdAndUpdate(prizeHistoryID,updateprizeHistory,{new: true},(error,data) =>{
            if(error){
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status: "Update user successfull",
                data: data
                })
            }
        )  
    } 
//   delete prize history by id : lấy lịch sử xúc xắc theo id
//   input: request params
//   output: delete dữ liệu từ csdl và reponse
const deletePrizeHistoryById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const prizeHistoryID = request.params.prizeHistoryId;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeHistoryID)){
        return response.status(400).json({
            status: "Bad Request",
            message: "Prize History Id không hợp lệ"
        })
    } 
    //B3:  Gọi model tạo dữ liệu
    prizeHistoryModel.findByIdAndDelete(prizeHistoryID, (error, data)=> {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(204).json({
            status: "Delete prize history successfully"
        })
    })
}
//export module controller
module.exports = {
    getAllPrizeHistory,
    createPrizeHistory,
    getPrizeHistoryByID,
    updatePrizeHistoryById,
    deletePrizeHistoryById
}