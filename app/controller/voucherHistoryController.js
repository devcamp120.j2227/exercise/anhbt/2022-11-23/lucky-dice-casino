// Import thư viện Mongoose
const mongoose = require("mongoose");
// Import module UserModel 
const voucherHistoryModel = require('../models/voucherHistoryModel');

/*** Get all voucher: lấy danh sách quay xúc xắc
 * nhận đầu vào request và response từ router
 * gọi voucherHistoryModel lấy tất cả dữ liệu user từ CSDL và trả về response.
*/
const getAllVoucherHistory = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let userId = request.params.user;
    let condition = { };
    console.log(userId)
    if (userId) {
        condition.user = userId;
    }
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    voucherHistoryModel.find(condition).exec((error, data)  => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all voucher history successfully",
            data: data
        })
    })
}
//   create voucher history : tạo lịch sử xúc xắc
//   input: request body json
//   output: tạo dữ liệu voucherHistory và lưu vào CSDL và trả về response 
const createVoucherHistory = (request, response) => {
    // B1: chuẩn bị dữ liệu
    const body = request.body;  
    // {
    //     "user": "637c907a3d1daf3956ad209a",
    //     "voucher" : 12
    // }
    // B2: kiểm tra dữ liệu
    if(!body.user || body.user.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "User  không hợp lệ!"
        })
    }
    if(isNaN(body.voucher) || body.voucher < 0) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Voucher không hợp lệ!"
        })
    }
    // B3: Thao tác CSDL
    //Tạo đối tượng chứa request
    const newVoucherHistory = {
        user: body.user,
        voucher: body.voucher
    }
    // Dùng create() thêm dữ liệu vào CSDL
    voucherHistoryModel.create(newVoucherHistory, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        //trả về response
        return response.status(201).json({
            status: "Successfully create a voucher history.",
            data: data
        })
    })
}
//   get voucher history by id : lấy lịch sử xúc xắc theo id
//   input: request params
//   output: lấy dữ liệu từ csdl và reponse
const getVoucherHistoryByID =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const voucherHistoryID =  request.params.voucherHistoryId
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherHistoryID)){
     return response.status(400).json({
             status: "Bad request",
             message: "voucherHistory Id không hợp lệ" 
         })
     }
     //bước 3: gọi model tạo dữ liêu
    voucherHistoryModel.findById(voucherHistoryID,(error,data) =>{
         return response.status(200).json({
             status: "Get detail voucher history successfull",
             data: data
             })
         }
     )  
}
//   update voucher history by id : lấy lịch sử xúc xắc theo id
//   input: request params
//   output: update dữ liệu từ csdl và reponse
const updateVoucherHistoryById =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const voucherHistoryID =  request.params.voucherHistoryId;
    const body = request.body;
    //bước 2: Validate dữ liệu
        if(!mongoose.Types.ObjectId.isValid(voucherHistoryID)){
            return response.status(400).json({
                    status: "Bad request",
                    message: "Voucher History Id không hợp lệ" 
                })
            }  
        if(body.user == undefined && body.user.trim() === ""){
            return response.status(400).json({
                status: "BAD REQUEST",
                message: "User không hợp lệ" 
            })
        }
        if(body.voucher == undefined && (isNaN(body.voucher) || body.voucher < 0)){
            return response.status(400).json({
                status: "BAD REQUEST",
                message: "Voucher không hợp lệ" 
            })
        }  
      const updatevoucherHistory= {
        user: body.user,
        voucher: body.voucher
        }
        //bước 3: gọi model tạo dữ liêu
        voucherHistoryModel.findByIdAndUpdate(voucherHistoryID,updatevoucherHistory,{new: true},(error,data) =>{
            if(error){
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status: "Update user successfull",
                data: data
                })
            }
        )  
    } 
//   delete voucher history by id : lấy lịch sử xúc xắc theo id
//   input: request params
//   output: delete dữ liệu từ csdl và reponse
const deleteVoucherHistoryById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const voucherHistoryID = request.params.voucherHistoryId;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherHistoryID)){
        return response.status(400).json({
            status: "Bad Request",
            message: "Voucher History Id không hợp lệ"
        })
    } 
    //B3:  Gọi model tạo dữ liệu
    voucherHistoryModel.findByIdAndDelete(voucherHistoryID, (error, data)=> {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(204).json({
            status: "Delete voucher history successfully"
        })
    })
}
//export module controller
module.exports = {
    getAllVoucherHistory,
    createVoucherHistory,
    getVoucherHistoryByID,
    updateVoucherHistoryById,
    deleteVoucherHistoryById
}