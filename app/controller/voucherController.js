// Import thư viện Mongoose
const mongoose = require("mongoose");
// Import module VoucherModel 
const voucherModel = require('../models/voucherModel');

/*** Get all voucher: lấy danh sách voucher
 * nhận đầu vào request và response từ router
 * gọi voucherModel lấy tất cả dữ liệu voucher từ CSDL và trả về response.
*/
const getAllVoucher = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    voucherModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all voucher successfully",
            data: data
        })
    })
}
/*** Create Voucher: tạo mới voucher
 * nhận đầu vào request và response từ router
 * lấy dữ liệu JSON từ request.body
 * gọi voucherModel thêm dữ liệu User vào CSDL và trả về response
*/
const createVoucher = (request, response) => {
    // B1: chuẩn bị dữ liệu
    const body = request.body;
    // {
    //     "code": "123456" ,
    //     "discount": 25,
    //      "note": "Hello"
    // }
    // B2: kiểm tra dữ liệu
    if(!body.code || body.code.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Code không hợp lệ!"
        })
    }
    if(!body.discount &&(isNaN(body.discount) || body.discount < 0)){
        return response.status(400).json({
            status: "Bad Request",
            message: "Discount không hợp lệ!"
        })
    }
    // B3: Thao tác CSDL
    //Tạo đối tượng chứa request
    const newVoucher = {
        code: body.code,
        discount: body.discount,
        note: body.note
    }
    // Dùng create() thêm dữ liệu vào CSDL
    voucherModel.create(newVoucher, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        //trả về response
        return response.status(201).json({
            status: "Create Voucher Successfully.",
            data: data
        })
    })
}
//   get voucher by id : lấy phần thưởng theo id
//   input: request params
//   output: lấy dữ liệu từ csdl và reponse
const getVoucherByID =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const voucherID =  request.params.voucherId;
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherID)){
     return response.status(400).json({
             status: "Bad request",
             message: "voucherId không hợp lệ" 
         })
     }
     //bước 3: gọi model tạo dữ liêu
    voucherModel.findById(voucherID,(error,data) =>{
         return response.status(200).json({
             status: "Get detail voucher successfull",
             data: data
             })
         }
     )  
}
//   update voucher by id : lấy voucher theo id
//   input: request params
//   output: update dữ liệu từ csdl và reponse
const updateVoucherById =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const voucherID =  request.params.voucherId;
    const body = request.body;
    //bước 2: Validate dữ liệu
        if(!mongoose.Types.ObjectId.isValid(voucherID)){
            return response.status(400).json({
                    status: "Bad request",
                    message: "Voucher Id không hợp lệ" 
                })
            }  
        if(body.code == undefined && body.code.trim() === ""){
            return response.status(400).json({
                status: "BAD REQUEST",
                message: "Code không hợp lệ" 
            })
        } 
        if(body.discount == undefined && (isNaN(body.discount) || body.discount < 0)){
            return response.status(400).json({
                status: "Bad Request",
                message: "Discount không hợp lệ!"
            })
        }
        const newVoucher = {
            code: body.code,
            discount: body.discount,
            note: body.note
        }
        //bước 3: gọi model tạo dữ liêu
        voucherModel.findByIdAndUpdate(voucherID,newVoucher,{new: true},(error,data) =>{
            if(error){
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status: "Update voucher successfull",
                data: data
                })
            }
        )  
}
//   delete voucher by id : lấy lịch sử xúc xắc theo id
//   input: request params
//   output: delete dữ liệu từ csdl và reponse
const deleteVoucherById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const voucherID =  request.params.voucherId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherID)){
        return response.status(400).json({
            status: "Bad Request",
            message: "voucher Id không hợp lệ"
        })
    } 
    //B3:  Gọi model tạo dữ liệu
    voucherModel.findByIdAndDelete(voucherID, (error, data)=> {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(204).json({
            status: "Delete voucher  successfully"
        })
    })
}
module.exports = {
    getAllVoucher,
    createVoucher,
    getVoucherByID,
    updateVoucherById,
    deleteVoucherById 
}