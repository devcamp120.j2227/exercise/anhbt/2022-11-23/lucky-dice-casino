// Import thư viện Mongoose
const mongoose = require("mongoose");
// Import module UserModel 
const diceHistoryModel = require('../models/diceHistoryModel');

/*** Get all dice: lấy danh sách quay xúc xắc
 * nhận đầu vào request và response từ router
 * gọi diceHistoryModel lấy tất cả dữ liệu user từ CSDL và trả về response.
*/
const getAllDiceHistory = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let userId = request.params.user;
    let condition = { };
    console.log(userId)
    if (userId) {
        condition.user = userId;
    }
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    diceHistoryModel.find(condition).exec((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all dice histories successfully",
            data: data
        })
    })
}
//   create dice history : tạo lịch sử xúc xắc
//   input: request body json
//   output: tạo dữ liệu diceHistory và lưu vào CSDL và trả về response 
const createDiceHistory = (request, response) => {
    // B1: chuẩn bị dữ liệu
    const body = request.body;
    
    // B2: kiểm tra dữ liệu
    if(!body.user || body.user.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "User  không hợp lệ!"
        })
    }
    // B3: Thao tác CSDL
    //Tạo đối tượng chứa request
    const newDiceHistory = {
        user: body.user,
        dice: body.dice
    }
    // Dùng create() thêm dữ liệu vào CSDL
    diceHistoryModel.create(newDiceHistory, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        //trả về response
        return response.status(201).json({
            status: "Successfully! Create a dice history.",
            data: data
        })
    })
}
//   get dice history by id : lấy lịch sử xúc xắc theo id
//   input: request params
//   output: lấy dữ liệu từ csdl và reponse
const getDiceHistoryByID =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const diceHistoryID =  request.params.diceHistoryId
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(diceHistoryID)){
     return response.status(400).json({
             status: "Bad request",
             message: "diceHistory Id không hợp lệ" 
         })
     }
     //bước 3: gọi model tạo dữ liêu
    diceHistoryModel.findById(diceHistoryID,(error,data) =>{
         return response.status(200).json({
             status: "Get detail dice history successfull",
             data: data
             })
         }
     )  
}
//   update dice history by id : lấy lịch sử xúc xắc theo id
//   input: request params
//   output: update dữ liệu từ csdl và reponse
const updateDiceHistoryById =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const diceHistoryID =  request.params.diceHistoryId;
    const body = request.body;
    //bước 2: Validate dữ liệu
        if(!mongoose.Types.ObjectId.isValid(diceHistoryID)){
            return response.status(400).json({
                    status: "Bad request",
                    message: "Dice History Id không hợp lệ" 
                })
            }  
        if(body.user == undefined && body.user.trim() === ""){
            return response.status(400).json({
                status: "BAD REQUEST",
                message: "User không hợp lệ" 
            })
        }
        if(body.bodyDice == undefined && (isNaN(body.bodyDice) || body.bodyDice < 0)){
            return response.status(400).json({
                status: "BAD REQUEST",
                message: "Dice không hợp lệ" 
            })
        }  
      const updatediceHistory= {
        user: body.user,
        dice: body.bodyDice
        }
        //bước 3: gọi model tạo dữ liêu
        diceHistoryModel.findByIdAndUpdate(diceHistoryID,updatediceHistory,{new: true},(error,data) =>{
            if(error){
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status: "Update user successfull",
                data: data
                })
            }
        )  
    } 
//   delete dice history by id : lấy lịch sử xúc xắc theo id
//   input: request params
//   output: delete dữ liệu từ csdl và reponse
const deleteDiceHistoryById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const diceHistoryID = request.params.diceHistoryId;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(diceHistoryID)){
        return response.status(400).json({
            status: "Bad Request",
            message: "Dice History Id không hợp lệ"
        })
    } 
    //B3:  Gọi model tạo dữ liệu
    diceHistoryModel.findByIdAndDelete(diceHistoryID, (error, data)=> {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(204).json({
            status: "Delete dice history successfully"
        })
    })
}
//export module controller
module.exports = {
    getAllDiceHistory,
    creatediceHistory: createDiceHistory,
    getDiceHistoryByID,
    updateDiceHistoryById,
    deleteDiceHistoryById
}