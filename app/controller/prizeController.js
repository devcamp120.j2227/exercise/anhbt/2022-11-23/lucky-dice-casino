// Import thư viện Mongoose
const mongoose = require("mongoose");
// Import module PrizeModel 
const prizeModel = require('../models/prizeModel');

/*** Get all prize: lấy danh sách phần thưởng
 * nhận đầu vào request và response từ router
 * gọi prizeModel lấy tất cả dữ liệu prize từ CSDL và trả về response.
*/
const getAllPrize = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    prizeModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all prize successfully",
            data: data
        })
    })
}
/*** Create Prize: tạo mới khách hàng
 * nhận đầu vào request và response từ router
 * lấy dữ liệu JSON từ request.body
 * gọi prizeModel thêm dữ liệu User vào CSDL và trả về response
*/
const createPrize = (request, response) => {
    // B1: chuẩn bị dữ liệu
    const body = request.body;
    // {
    //     "name": "Car" ,
    //     "description": "Nothing"    
    // }
    // B2: kiểm tra dữ liệu
    if(!body.name || body.name.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: " Name không hợp lệ!"
        })
    }
    
    // B3: Thao tác CSDL
    //Tạo đối tượng chứa request
    const newPrize = {
        name: body.name,
        description: body.description,
    }
    // Dùng create() thêm dữ liệu vào CSDL
    prizeModel.create(newPrize, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        //trả về response
        return response.status(201).json({
            status: "Create Prize Successfully.",
            data: data
        })
    })
}
//   get prize by id : lấy phần thưởng theo id
//   input: request params
//   output: lấy dữ liệu từ csdl và reponse
const getPrizeByID =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const prizeID =  request.params.prizeId;
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeID)){
     return response.status(400).json({
             status: "Bad request",
             message: "prizeId không hợp lệ" 
         })
     }
     //bước 3: gọi model tạo dữ liêu
    prizeModel.findById(prizeID,(error,data) =>{
         return response.status(200).json({
             status: "Get detail prize successfull",
             data: data
             })
         }
     )  
}
//   update dice history by id : lấy lịch sử xúc xắc theo id
//   input: request params
//   output: update dữ liệu từ csdl và reponse
const updatePrizeById =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const prizeID =  request.params.prizeId;
    const body = request.body;
    //bước 2: Validate dữ liệu
        if(!mongoose.Types.ObjectId.isValid(prizeID)){
            return response.status(400).json({
                    status: "Bad request",
                    message: "Dice History Id không hợp lệ" 
                })
            }  
        if(body.name == undefined && body.name.trim() === ""){
            return response.status(400).json({
                status: "BAD REQUEST",
                message: "Name không hợp lệ" 
            })
        } 
        const newPrize = {
            name: body.name,
            description: body.description,
        }
        //bước 3: gọi model tạo dữ liêu
        prizeModel.findByIdAndUpdate(prizeID,newPrize,{new: true},(error,data) =>{
            if(error){
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status: "Update prize successfull",
                data: data
                })
            }
        )  
}
//   delete prize by id : lấy lịch sử xúc xắc theo id
//   input: request params
//   output: delete dữ liệu từ csdl và reponse
const deleteDiceById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const prizeID =  request.params.prizeId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeID)){
        return response.status(400).json({
            status: "Bad Request",
            message: "Prize Id không hợp lệ"
        })
    } 
    //B3:  Gọi model tạo dữ liệu
    prizeModel.findByIdAndDelete(prizeID, (error, data)=> {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(204).json({
            status: "Delete prize  successfully"
        })
    })
}
module.exports = {
    getAllPrize,
    createPrize,
    getPrizeByID,
    updatePrizeById,
    deleteDiceById 
}